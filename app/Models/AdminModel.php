<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2019 南京RXThink工作室
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <rxthink.cn@gmail.com>
// +----------------------------------------------------------------------

namespace App\Models;

/**
 * 人员管理-模型
 * @author zongjl
 * @date 2019/5/23
 * Class AdminModel
 * @package App\Models
 */
class AdminModel extends BaseModel
{
    // 设置数据表
    protected $table = 'yh_admin';

    /**
     * 获取缓存信息
     * @param int $id 记录ID
     * @return array 返回结果
     * @author zongjl
     * @date 2019/6/4
     */
    public function getInfo($id)
    {
        // 清除缓存
        $this->cacheDelete($id);
        $info = parent::getInfo($id, true);
        if ($info) {
            // TODO...
        }
        return $info;
    }
}
