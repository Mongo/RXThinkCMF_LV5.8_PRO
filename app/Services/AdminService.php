<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2019 南京RXThink工作室
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <rxthink.cn@gmail.com>
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\AdminModel;
use Validator;

/**
 * 人员管理-服务类
 * @author zongjl
 * @date 2019/5/23
 * Class AdminService
 * @package App\Services
 */
class AdminService extends BaseService
{
    /**
     * 构造方法
     * @author zongjl
     * @date 2019/5/23
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new AdminModel();
    }

    /**
     * 系统登录
     * @param $request 网络请求
     * @return array 返回结果
     * @author zongjl
     * @date 2019/5/24
     */
    public function login($request)
    {
        // 数据验证
        $validator = Validator::make($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        // 参数
        $username = trim($request['username']);
        $password = trim($request['password']);

        // 获取用户信息
        $info = $this->model->getOne([
            ['username', '=', $username],
        ]);
        if (!$info) {
            return message("您的账号不存在");
        }

        // 密码校验
        if (get_password($password . $username) != $info['password']) {
            return message("密码不正确");
        }

        // 人员状态验证
        if ($info['status'] != 1) {
            return message("您的账号已被禁用,请联系管理员");
        }

        // 生成token
        $token = get_rand_code();
        $rowId = $this->model->edit([
            'id' => $info['id'],
            'token' => $token,
        ]);
        if (!$rowId) {
            return message("登录TOKEN更新失败");
        }

        // 结果返回
        $result = [
            'token' => $token,
        ];
        return message(MESSAGE_OK, true, $result);
    }
}
