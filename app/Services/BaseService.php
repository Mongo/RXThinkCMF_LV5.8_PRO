<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2019 南京RXThink工作室
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <rxthink.cn@gmail.com>
// +----------------------------------------------------------------------

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

/**
 * 服务类-基类
 * @author zongjl
 * @date 2019/5/23
 * Class BaseService
 * @package App\Services
 */
class BaseService extends Model
{
    // 模型
    protected $model;
    // 验证类
    protected $validate;

    /**
     * 构造方法
     * @author zongjl
     * @date 2019/5/23
     */
    public function __construct()
    {
        parent::__construct();
        // TODO...
    }

    /**
     * 分页参数设置
     * @param int $page 页码
     * @param int $perpage 每页数
     * @param string $limit 限制条数
     * @author zongjl
     * @date 2019/5/24
     */
    public function initPage(&$page, &$perpage, &$limit)
    {
        $page = (int)$_REQUEST['page'];
        $perpage = (int)$_REQUEST['perpage'];
        $page = $page ? $page : 1;
        $perpage = $perpage ? $perpage : 10;
        $startIndex = ($page - 1) * $perpage;
        $limit = "{$startIndex}, {$perpage}";
    }
}
