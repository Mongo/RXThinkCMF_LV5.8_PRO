<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2019 南京RXThink工作室
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <rxthink.cn@gmail.com>
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Services\AdminService;

/**
 * 系统登录-控制器
 * 
 * @author zongjl
 * @date 2019-05-24
 *
 */
class LoginController extends BaseController
{
    /**
     * 构造方法
     * 
     * @author zongjl
     * @date 2019-05-24
     * @param Request $request
     */
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->service = new AdminService();
    }
    
    /**
     * 登录API
     * 
     * @author zongjl
     * @date 2019-05-24
     */
    function login()
    {
        $result = $this->service->login($this->request);
        $this->jsonReturn($result);
    }
    
}