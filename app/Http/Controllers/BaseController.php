<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2019 南京RXThink工作室
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <rxthink.cn@gmail.com>
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\AdminModel;

/**
 * 基类控制器
 *
 * @author zongjl
 * @date 2019/5/23
 */
// 跨域问题解决方案
header("Access-Control-Allow-Origin:*");
class BaseController extends Controller
{
    // 模型
    protected $model;
    // 服务
    protected $service;
    // 验证
    protected $validate;
    // 网络请求
    protected $request;
    // 请求序列号(系统生成)
    protected $request_id;
    // 鉴权Token
    protected $token;
    // 人员ID
    protected $adminId;
    // 人员信息
    protected $adminInfo;

    /**
     * 构造函数
     * BaseController constructor.
     * @param Request $request
     * @author zongjl
     * @date 2019/5/23
     */
    public function __construct(Request $request)
    {
        // 初始化网络请求配置
        $this->initRequestConfig($request);

        // 初始化系统常量
        $this->initSystemConst();

        // 初始化请求信息
        $this->initRequestInfo($request);
    }

    /**
     * 初始化请求信息
     * @param $request
     * @author zongjl
     * @date 2019/5/24
     */
    private function initRequestInfo($request)
    {
        // 请求序列编码
        $requestId = substr(md5(time() . get_random_code(10) . rand(1, 1000)), 8, 16);
        $this->request_id = $requestId;

        // 请求参数
        $this->request = $request->all();

        // 登录时无需进行Token验证
        if (CONTROLLER_NAME != 'login') {
            // 请求token
            $token = isset($this->request['token']) ? $this->request['token'] : '';
            if (!$token) {
                $this->jsonReturn(message("请传token"));
            }

            // 获取用户信息
            $admin_model = new AdminModel();
            $adminInfo = $admin_model->getOne([
                ['token', '=', $token],
            ]);
            if (!$adminInfo) {
                $this->jsonReturn(message("您的信息不存在"));
            }

            // 状态校验
            if ($adminInfo['status'] != 1) {
                $this->jsonReturn(message("您已经被禁用，请联系管理员"));
            }

            // 登录用户ID及信息
            $this->adminId = $adminInfo['id'];
            $this->adminInfo = $adminInfo;
        }
    }

    /**
     * 初始化请求配置
     * @param Request $request 网络请求
     * @author zongjl
     * @date 2019/5/23
     */
    private function initRequestConfig(Request $request)
    {

        // 定义是否GET请求
        defined('IS_GET') or define('IS_GET', $request->isMethod('GET'));

        // 定义是否POST请求
        defined('IS_POST') or define('IS_POST', $request->isMethod('POST'));

        // 定义是否AJAX请求
        defined('IS_AJAX') or define('IS_AJAX', $request->ajax());

        // 定义是否PAJAX请求
        defined('IS_PJAX') or define('IS_PJAX', $request->pjax());

        // 定义是否PUT请求
        defined('IS_PUT') or define('IS_PUT', $request->isMethod('PUT'));

        // 定义是否DELETE请求
        defined('IS_DELETE') or define('IS_DELETE', $request->isMethod('DELETE'));

        // 请求方式
        defined('REQUEST_METHOD') or define('REQUEST_METHOD', $request->method());

        // 请求地址
        list($controller, $action) = explode('/', $request->path());
        // 控制器名
        defined('CONTROLLER_NAME') or define('CONTROLLER_NAME', $controller);
        // 方法名
        defined('ACTION_NAME') or define('ACTION_NAME', $action);
    }

    /**
     * 初始化系统常量
     * @author zongjl
     * @date 2019/5/23
     */
    private function initSystemConst()
    {
        // 项目根目录
        defined('ROOT_PATH') or define('ROOT_PATH', base_path());

        // 文件上传目录
        defined('ATTACHMENT_PATH') or define('ATTACHMENT_PATH', base_path('public/uploads'));

        // 图片上传目录
        defined('IMG_PATH') or define('IMG_PATH', base_path('public/uploads/img'));

        // 临时存放目录
        defined('UPLOAD_TEMP_PATH') or define('UPLOAD_TEMP_PATH', IMG_PATH . "/temp");

        // 定义普通图片域名
        defined('IMG_URL') or define('IMG_URL', 'http://img.lumen.com');

        // 数据表前缀
        define('DB_PREFIX', \DB::getTablePrefix());
    }

    /**
     * 登录验证(接口需要登录时调用此方法)
     * @author zongjl
     * @date 2019/5/24
     */
    protected function needLogin()
    {
        if (!$this->adminId) {
            $this->jsonReturn(message("请登录"));
        }
    }

    /**
     * 初始化分页设置
     * @param int $page 页码
     * @param int $perpage 每页数
     * @param string $limit 限制条数
     * @author zongjl
     * @date 2019/5/24
     */
    public function initPage(&$page, &$perpage, &$limit)
    {
        $page = (int)$this->req['page'];
        $perpage = (int)$this->req['perpage'];
        $page = $page ? $page : 1;
        $perpage = $perpage ? $perpage : 10;
        $startIndex = ($page - 1) * $perpage;
        $limit = "{$startIndex}, {$perpage}";
    }

    /**
     * JSON返回结果
     * @author zongjl
     * @date 2019/5/28
     */
    public function jsonReturn()
    {
        false && message();

        // 获取参数
        $arr = func_get_args();

        if (!is_array($arr[0])) {
            // 回调函数
            $result = call_user_func_array("message", $arr);
        } else {
            // 函数模式
            $result = $arr[0];
        }

        // Mongodb请求日志入库存储(后期规划中...)

        // 返回结果
        $output = json_encode($result);
        echo $output;
        exit();
    }

    /**
     * 默认入口页
     * @author zongjl
     * @date 2019/6/3
     */
    public function index()
    {
        if (IS_POST) {
            $result = $this->service->getList();
            return $result;
        }
        // 默认参数
        $data = func_get_args();
        if ($data) {
            foreach ($data as $key => $val) {
                $this->assign($key, $val);
            }
        }
        return $this->render();
    }

    /**
     * 添加或编辑
     * @param Request $request
     * @return view 页面渲染
     * @author zongjl
     * @date 2019/6/3
     */
    public function edit(Request $request)
    {
        if (IS_POST) {
            $result = $this->service->edit();
            return $result;
        }
        $info = [];
        $id = $request->input('id', 0);
        if ($id) {
            $info = $this->model->getInfo($id);
        } else {
            $data = func_get_args();
            if ($data) {
                foreach ($data as $key => $val) {
                    $info[$key] = $val;
                }
            }
        }
        $this->assign('info', $info);
        return $this->render();
    }

    /**
     * 获取详情
     * @param Request $request 网络请求
     * @return view 页面渲染
     * @author zongjl
     * @date 2019/6/3
     */
    public function detail(Request $request)
    {
        if (IS_POST) {
            $result = $this->service->edit();
            return $result;
        }
        $id = $request->input("id", 0);
        if ($id) {
            $info = $this->model->getInfo($id);
            $this->assign('info', $info);
        }
        return $this->render();
    }

    /**
     * 删除
     * @param Request $request 网络请求
     * @return array 返回结果
     * @author zongjl
     * @date 2019/6/3
     */
    public function drop(Request $request)
    {
        if (IS_POST) {
            $id = $request->input('id', 0);
            $info = $this->model->getInfo($id);
            if ($info) {
                $result = $this->model->drop($id);
                if ($result !== false) {
                    return message();
                }
            }
            return message($this->model->getError(), false);
        }
    }

    /**
     * 重置缓存
     * @param Request $request 网络请求
     * @return array 返回结果
     * @author zongjl
     * @date 2019/6/3
     */
    public function cache(Request $request)
    {
        if (IS_POST) {
            $id = $request->input('id', 0);
            if (!$id) {
                return message("记录ID不能为空", false);
            }
            $result = $this->model->_cacheReset($id, '');
            if (!$result) {
                return message("缓存重置失败", false);
            }
            return message("缓存重置成功");
        }
    }

    /**
     * 一键复制
     * @param Request $request 网络请求
     * @return view 页面渲染
     * @author zongjl
     * @date 2019/6/3
     */
    public function copy(Request $request)
    {
        if (IS_POST) {
            $result = $this->service->edit();
            return $result;
        }
        $info = [];
        $id = $request->input("id", 0);
        if ($id) {
            $info = $this->model->getInfo($id);
        } else {
            $data = func_get_args();
            if ($data) {
                foreach ($data[0] as $key => $val) {
                    $info[$key] = $val;
                }
            }
        }
        // 复制作为新增数据，主键ID必须置空
        unset($info['id']);
        $this->assign('info', $info);
        return $this->render('edit');
    }

    /**
     * 更新单个字段
     * @param Request $request 网络请求
     * @return array 返回结果
     * @author zongjl
     * @date 2019/6/3
     */
    public function update(Request $request)
    {
        if (IS_POST) {
            $data = $request->all();
            $result = $this->model->edit($data);
            if ($result) {
                return message("更新成功");
            } else {
                return message("更新失败", false);
            }
        }
    }

    /**
     * 渲染模板
     * @param string $view 模板路径
     * @param array $data 参数
     * @return view 模板渲染
     * @author zongjl
     * @date 2019/6/3
     */
    public function render($view = "", $data = [])
    {
        return view($view, $data);
    }

    /**
     * 批量删除记录
     * @return array 返回结果
     * @author zongjl
     * @date 2019/6/3
     */
    public function batchDrop()
    {
        if (IS_POST) {
            $ids = explode(',', $_POST['id']);
            //批量删除
            $num = 0;
            foreach ($ids as $key => $val) {
                $res = $this->model->drop($val);
                if ($res !== false) {
                    $num++;
                }
            }
            return message('本次共选择' . count($ids) . "个条数据,删除" . $num . "个");
        }
    }

    /**
     * 批量重置缓存
     * @return array 返回结果
     * @author zongjl
     * @date 2019/6/3
     */
    public function batchCache()
    {
        if (IS_POST) {
            $ids = explode(',', $_POST['id']);
            //重置缓存
            foreach ($ids as $key => $val) {
                $this->model->_cacheReset($val);
            }
            return message('重置缓存成功！');
        }
    }

    /**
     * 批量设置状态
     * @return array 返回结果
     * @author zongjl
     * @date 2019/6/3
     */
    public function batchStatus()
    {
        if (IS_POST) {
            $ids = explode(',', $_POST['id']);
            $status = (int)$_POST['status'];
            if (!is_array($ids)) {
                return message("请选择数据记录", false);
            }
            $num = 0;
            foreach ($ids as $key => $val) {
                $result = $this->model->edit([
                    'id' => $val,
                    'status' => $status,
                ]);
                if ($result) {
                    $num++;
                }
            }
            return message("本次共更新【{$num}】条记录");
        }
    }
}
