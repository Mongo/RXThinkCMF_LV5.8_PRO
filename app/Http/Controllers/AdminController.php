<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2019 南京RXThink工作室
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <rxthink.cn@gmail.com>
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use App\Models\AdminModel;
use App\Services\AdminService;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/**
 * 人员-控制器
 * @author zongjl
 * @date 2019/5/23
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends BaseController
{
    /**
     * 构造方法
     * AdminController constructor.
     * @param Request $request
     * @author zongjl
     * @date 2019/5/23
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new AdminModel();
        $this->service = new AdminService();
    }

    /**
     * 首页入口
     * @return \Illuminate\View\View
     * @author zongjl
     * @date 2019/5/23
     */
    public function index()
    {
        return $this->render();
    }
}
